import { commitMutation, graphql, Variables } from 'react-relay'
import { MutationCallbacks } from '@saastack/relay'
import { Disposable, Environment, RecordSourceSelectorProxy } from 'relay-runtime'
import {
    DeleteDepartmentInput,
    DeleteDepartmentMutation,
} from '../__generated__/DeleteDepartmentMutation.graphql'

const mutation = graphql`
    mutation DeleteDepartmentMutation($input: DeleteDepartmentInput) {
        deleteDepartment(input: $input) {
            clientMutationId
        }
    }
`

const sharedUpdater = (store: RecordSourceSelectorProxy, id: string, filters: Variables) => {
    const root = store.getRoot();
    const listProxy = root.getLinkedRecord('departments',filters)
    if(listProxy){
        const recordProxy = listProxy.getLinkedRecords('department')
        listProxy.setLinkedRecords((recordProxy||[]).filter(item=>item.getDataID() !== id),'department')
    }  
}

let tempID = 0

const commit = (
    environment: Environment,
    variables: Variables,
    id: string,
    callbacks?: MutationCallbacks<string>
): Disposable => {
    const input: DeleteDepartmentInput = {
        id,
        clientMutationId: `${tempID++}`,
    }

    return commitMutation<DeleteDepartmentMutation>(environment, {
        mutation,
        variables: {
            input,
        },
        updater: (store: RecordSourceSelectorProxy) => sharedUpdater(store, id, variables),
        onError: (error: Error) => {
            if (callbacks && callbacks.onError) {
                const message = error.message.split('\n')[1]
                callbacks.onError!(message)
            }
        },
        onCompleted: () => {
            if (callbacks && callbacks.onSuccess) {
                callbacks.onSuccess(id)
            }
        },
    })
}

export default { commit }
