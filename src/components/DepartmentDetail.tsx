import React from 'react'
import { createFragmentContainer, graphql } from 'react-relay'
import { useNavigate, useParams } from '@saastack/router'
import { DepartmentDetail_departments } from '../__generated__/DepartmentDetail_departments.graphql'
import { Roles, useCan } from '@saastack/core/roles'
import { ActionItem } from '@saastack/components'
import { Trans } from '@lingui/macro'
import { CloseOutlined, DeleteOutlined, EditOutlined } from '@material-ui/icons'
import { DetailContainer } from '@saastack/layouts/containers'
import { CardHeader } from '@material-ui/core'
import hooks from '@saastack/hooks'
import { TabHook } from '@saastack/hooks'
import DepartmentInfo from './DepartmentInfo'
import { useConfig } from '@saastack/core'

interface Props {
    departments: DepartmentDetail_departments
}

const DepartmentDetail: React.FC<Props> = ({ departments, ...props }) => {
    const navigate = useNavigate()
    const [open, setOpen] = React.useState(true)
    const can = useCan()
    const { activeApps } = useConfig()

    const { id } = useParams()
    const department = departments.find((i) => i.id === window.atob(id!))

    const navigateBack = () => navigate('../../')
    const handleClose = () => setOpen(false)
    React.useEffect(() => {
        if (!department) {
            handleClose()
        }
    }, [department])
    if (!department) {
        return null
    }

    const admin = can([Roles.DepartmentsAdmin], department.id)
    const editor = can([Roles.DepartmentsAdmin, Roles.DepartmentsEditor], department.id)

    const header = <CardHeader title={<Trans>Title</Trans>} />
    const actions: ActionItem[] = [
        ...(editor
            ? [
                  {
                      title: <Trans>Update</Trans>,
                      icon: EditOutlined,
                      onClick: () => navigate('update'),
                  },
                  ...(admin
                      ? [
                            {
                                title: <Trans>Delete</Trans>,
                                icon: DeleteOutlined,
                                onClick: () => navigate(`delete`),
                            },
                        ]
                      : []),
              ]
            : []),
        { title: <Trans>Close</Trans>, icon: CloseOutlined, onClick: handleClose },
    ]

    const hookItems = hooks.department.tab.getAllHooks(activeApps)
    const tabs: TabHook[] = [
        { label: <Trans>Info</Trans>, component: DepartmentInfo as any },
        ...hookItems,
    ]
    const tabProps = {
        parent: department.id,
        department,
    }

    return (
        <DetailContainer
            boxed
            open={open}
            onClose={handleClose}
            onExited={navigateBack}
            actions={actions}
            header={header}
            tabProps={tabProps}
            tabs={tabs}
            {...props}
        />
    )
}

export default createFragmentContainer(DepartmentDetail, {
    departments: graphql`
        fragment DepartmentDetail_departments on Department @relay(plural: true) {
            id
            ...DepartmentInfo_department
        }
    `,
})
